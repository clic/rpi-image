#!/bin/bash -e

echo "${TARGET_HOSTNAME}" > "${ROOTFS_DIR}/etc/hostname"
echo "127.0.1.1		${TARGET_HOSTNAME}" >> "${ROOTFS_DIR}/etc/hosts"

on_chroot << EOF
	SUDO_USER="${FIRST_USER_NAME}" raspi-config nonint do_net_names 1
EOF

# For some reason some deb mirrors CA cert are not regognized
# and this is fixed by adding a ~/.curlrc file ...
on_chroot << EOF
echo  capath=/etc/ssl/certs/ > /root/.curlrc
echo cacert=/etc/ssl/certs/ca-certificates.crt >> /root/.curlrc
EOF
