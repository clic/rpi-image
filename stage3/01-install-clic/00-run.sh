#!/bin/bash -e

# Install CLIC post install app
on_chroot << EOF
if [ -d /var/www/install_clic ]; then rm -rf /var/www/install_clic; fi
git clone https://framagit.org/clic/clic.git /var/www/install_clic
cd /var/www/install_clic
source deploy/deploy.sh
EOF
